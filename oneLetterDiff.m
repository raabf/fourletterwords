function [ bool ] = oneLetterDiff( w1, w2 )
%% ONELETTERDIFF Check if `w1` and `w2` differs in only one letter
% @ret bool: true if `w1` and `w2` differs in only one letter
%            false else

% counts the letters which differ
counter=0;

for i = 1:4
    if w1(i) ~= w2(i)
        counter= counter + 1;
    end
    
end

bool = (counter == 1);

end

