function [ t, found ] = breadthSearchIter( t, node, aim, dict, full )
%% BREADTHSEARCHITER Does a breadth first search to find a possible deriviation for 'aim'
% Traverse and create the word tree iteratively. Returns if 'aim'
% is found or no other deriviation is possible. 
% @param t: actual tree
% @param node: the node we want to process in this function
% @param aim: The word we want to create
% @param dict: dictionary with vaild words
% @param full: true if the full tree should be calculated even after the
%                   aim is found
%              false stop when the aim is found
% @ret t: the search tree, which includes the solution path
% @ret found: is set to the node of the solution
%                or 0 if no solution is possible.

% Create Process Bar
h = waitbar(0,'1','Name',[t.get(node), ' -> ..?.. -> ', aim] );

found = 0;

%% Loop trough all Nodes
while node <= t.nnodes()
    
    [t, foundLoop] = dictSearch( t, node, aim, dict );
    
    %% Check if we found our aim
    if foundLoop ~= 0
        found = foundLoop;
        if ~full
            break;
        end
    end
    
    %% Process the next node
    % simply +1 because we build the tree directly in breath search order
    node = node + 1;
    
    waitbar(node / size(dict,1),h,sprintf('node number: %d',node))
end

%% Close Process Bar
waitbar(1,h,sprintf('Please Wait ...'))
delete(h)
end

