WS14/15 - Künstliche Intellegenz - Competition
==========================================
Assignment 2 - Question 2.4.5
------------------------------

Word Ladder is a game where the player is given a start word (e.g. COLD) and a goal word (e.g. WARM). By changing one letter at a time, the player turns the start word into the goal word. Each step must be a valid word.

A Breath First Search will find the solution in this matlab program.

Authors
-------
 + Faban Raab <fabian.raab@tum.de>
 + Robert Posch <posch.robert@gmail.com>
 + Ann Katrin Gibtner <annkatrin.gibtner@tum.de>

Result
------

You find more detailed output in `result.txt`.

| START | AIM  | PATH                                                  |
| ---   | ---  | ----------------------------------------------------- |
| KALT  | WARM | KALT-> HALT-> HART-> WART-> WARM                      |
| ZEIT  | GELD | ZEIT-> REIT-> REIM-> HEIM-> HELM-> HELD-> GELD        |
| BIER  | FASS | BIER-> BIET-> BIST-> BAST-> BASS-> FASS               |
| BLUT  | FELS | BLUT-> BAUT-> BAUD-> BALD-> BALL-> FALL-> FELL-> FELS |
| HUND  | HASE | HUND-> HAND-> HANS-> HASS-> HASE                      |
| HAUS  | HOLZ | HAUS-> HALS-> HALM-> HOLM-> HOLZ                      |
| GOLD  | BLEI | **not possible!**                                     |
| KOPF  | BEIN | KOPF-> TOPF-> TORF-> DORF-> DORN-> KORN-> KERN-> BERN-> BEIN            |
| BERG  | MEER | BERG-> BERT-> WERT-> WEHT-> WEHR-> MEHR-> MEER        |

Running
------

Just change in Matlab to the dir where `fourLetterWords.m` is located, run the script `fourLetterWords.m` and see the flavour! The script stops if a result is found. So it doesn't construct the rest of the tree.

Tree Plots
--------

The Script `fourLetterWords.m` produces plots of the Trees. You can find them already saved in the root folder named `[Nr][ROOT]-[AIM].fig`. Here is an example:
![5HUND-HASE.png](https://bitbucket.org/raabf/fourletterwords/raw/master/5HUND-HASE.png)

Sources
------

We use an already existing tree structure called [Matlab Tree](http://tinevez.github.io/matlab-tree/) from [Jean-Yves Tinevez](https://github.com/tinevez). So the code in the `@tree` subfolder is not from us.