function [ t, found ] = dictSearch( t, node, aim, dict)
%% DICTSEARCH Search the dict for possible next words and add them as leafs
%  to `node`
% @param t: actual tree
% @param node: the node we want to process in this function
% @param dict: dictionary with vaild words
% @ret found: == 0 if the aim is not found
%             ~= 0 if the aim is found. `found` is then the aim-node.
% @ret t: The actual tree with additional nodes

found = 0;

%% iterate trough dic
i = 0;
while i < size(dict, 1)
     i = i + 1;
    
    word = dict{i,1};
    
    %% decides if word differs with one letter
    if oneLetterDiff(t.get(node), word)
        
       % if word is already explored don't add to tree
       if any(strncmp(t,word,4))
           continue
       end
       
       %% add found word to tree
       [t, addedNode] = t.addnode(node, word);
       
       % have we found our aim?
       if strcmp(aim, word)
            found = addedNode;
            return
       end
    end
    
end

end

