function [ t, npath, tpath ] = wordLadder( start, aim, full )
%WORDLADDER Solves the Word Ladder game and plot the tree.
% @param start: The start word
% @param aim: the goal/aim word of the game
% @param full: true if the full tree should be calculated even after the
%                   aim is found
%              false stop when the aim is found
% @ret t: the search tree, which includes the solution path
% @ret npath: The path in the tree as nodes
% @ret tpath: The path in the tree as textual (tostring ready)
% representation.

load fourLetterWordsDict.mat;

found = 0;

% root of tree is the beginning word
t = tree(start);
disp(['###### ', t.get(1), ' -> ..?.. -> ', aim, ' ######']);

%% iterative
 [ t, found ] = breadthSearchIter(t, 1, aim, fourLetterWordsDict, full);

%% draw tree
disp(t.tostring);

figure('Name',[t.get(1), ' -> ..?.. -> ', aim])
[vlh, hlh, tlh] = t.plot([], 'TextRotation', 90);
set( tlh.get(1), 'Color' , 'red' );
set( vlh.get(1), 'Color' , 'red' );
set( hlh.get(1), 'Color' , 'red' );

%% get and print path
if found ~= 0
    npath = t.findpath(1, found);

    % Get Text of nodes and colering plot
    tpath = t.get(npath(1));
    for i = 2:size(npath, 2)
        tpath = [ tpath, '-> ', t.get(npath(i))];

        %coloring plot
        set( tlh.get(npath(i)), 'Color' , 'red' );
        set( vlh.get(npath(i)), 'Color' , 'red' );
        set( hlh.get(npath(i)), 'Color' , 'red' );
        xlabel(tpath); 
    end

    disp(' ');
    disp(['Solution: ',tpath]);

else
    disp('not possible!');
    npath = 1;
    tpath = 'not possible!';
end

end

