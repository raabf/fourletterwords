function [ t, found ] = breadthSearchRec( t, node, aim, dict, frontier)
%% BREADTHSEARCHREC Does an breath search to find a explored to `aim`
% (recursve). maybe this reach a maximum recursion depth limit.
% @param t: actual tree
% @param node: the node we want to process in this function
% @param aim: The word we want to create
% @param dict: dictionary with vaild words
% @param frontier: List of frontier words

[t, found, frontier] = dictSearch( t, node, aim, dict, frontier );


if found ~= 0
    return
end
if size(frontier) == 0
   return 
end
% simply +1 because we build the tree directly in breath search order
[ t, found ] = breadthSearchRec(t, node+1, aim, dict, frontier);
end

