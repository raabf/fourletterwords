taskWords = {
 'KALT', 'WARM', '?';
 'ZEIT', 'GELD', '?';
 'BIER', 'FASS', '?';
 'BLUT', 'FELS', '?';
 'HUND', 'HASE', '?';
 'HAUS', 'HOLZ', '?';
 'GOLD', 'BLEI', '?';
 'KOPF', 'BEIN', '?';
 'BERG', 'MEER', '?'};

diary('result.txt');
diary on;

for task = 1:1 %size(taskWords,1)

    tic
    found = 0;

    % root of tree is the beginning word
    start = taskWords{task,1};
    aim = taskWords{task,2};
    
    [ t, npath, tpath ] = wordLadder( start, aim, false );
    
    taskWords{task,3} = tpath;
    
    toc
    
    % display results
    diary off;
    disp(' ');
    disp('    START     AIM       PATH');
    disp(taskWords);
    disp(' ');
    diary on;

end

% display final results
disp(' ###### FINAL RESULTS ###### ');
disp('    START     AIM       PATH');
disp(taskWords);
disp(' ');

diary off;
